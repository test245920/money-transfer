<?php

namespace App\Tests\Feature\Controller;

use App\DataFixtures\TransactionFixture;
use App\DataFixtures\UserFixture;
use App\Entity\User;
use App\Repository\TransactionsRepository;
use App\Repository\UserRepository;
use App\Tests\Feature\WebTestCase;
use Symfony\Component\HttpFoundation\Response;

class TransactionControllerTest extends WebTestCase
{
    private TransactionsRepository $transactionsRepository;
    protected UserRepository $userRepository;

    protected function setUp(): void
    {
        parent::setUp();

        $this->transactionsRepository = $this->container->get(TransactionsRepository::class);
        $this->userRepository = $this->container->get(UserRepository::class);
    }

    public function testSearchTransactionsForAdmin(): void
    {
        $this->loginAdmin();

        $this->loadFixture(TransactionFixture::class, count: 9);

        $this->client->request('GET', '/api/transactions');

        $responseData = json_decode($this->client->getResponse()->getContent(), true);

        $this->assertResponseIsSuccessful();
        $this->assertIsArray($responseData);
        $this->assertArrayHasKey('data', $responseData);
        $this->assertCount(9, $responseData['data']);

        foreach ($responseData['data'] as $transaction) {
            $this->assertArrayHasKey('id', $transaction);
            $this->assertArrayHasKey('sender', $transaction);
            $this->assertArrayHasKey('receiver', $transaction);
            $this->assertArrayHasKey('amount', $transaction);
            $this->assertArrayHasKey('createdAt', $transaction);
        }
    }

    public function testSearchTransactionsForUser(): void
    {
        $this->loginUser();
        $authUser = $this->userRepository->findOneByEmail(self::AUTH_USER_EMAIL);
        $this->loadFixture(TransactionFixture::class, count: 9, append: true);
        $this->loadFixture(TransactionFixture::class, ['sender_id' => $authUser->getId()], count: 3, append: true);

        $this->client->request('GET', '/api/transactions');

        $responseData = json_decode($this->client->getResponse()->getContent(), true);

        $this->assertResponseIsSuccessful();
        $this->assertIsArray($responseData);
        $this->assertArrayHasKey('data', $responseData);
        $this->assertCount(3, $responseData['data']);

        foreach ($responseData['data'] as $transaction) {
            $this->assertArrayHasKey('id', $transaction);
            $this->assertArrayHasKey('sender', $transaction);
            $this->assertArrayHasKey('receiver', $transaction);
            $this->assertArrayHasKey('amount', $transaction);
            $this->assertArrayHasKey('createdAt', $transaction);
        }
    }

    public function testGetTransactionsListValidationFailed(): void
    {
        $this->loginUser();

        $this->client->request('GET', '/api/transactions',['maxAmount' => 'string']);

        $response = $this->client->getResponse();
        $responseData = json_decode($response->getContent(), true);

        $this->assertSame(Response::HTTP_UNPROCESSABLE_ENTITY, $response->getStatusCode());
        $this->assertArrayHasKey('message', $responseData);
        $this->assertArrayHasKey('errors', $responseData);
    }

    public function testGetTransactionsListUnauthorized(): void
    {
        $this->client->request('GET', '/api/transactions');

        $this->assertSame(Response::HTTP_UNAUTHORIZED, $this->client->getResponse()->getStatusCode());
    }

    public function testGetTransactionDetailsSuccessForAdmin()
    {
        $this->loginAdmin();

        $this->loadFixture(TransactionFixture::class, count:5);
        $lastTransaction = $this->transactionsRepository->findAll()[rand(0,4)];

        $this->client->request('GET', '/api/transactions/' . $lastTransaction->getId());

        $response = $this->client->getResponse();

        $this->assertSame(Response::HTTP_OK, $response->getStatusCode());
    }

    public function testGetTransactionDetailsSuccessForUser(): void
    {
        $this->loginUser();

        $user = $this->userRepository->findOneByEmail(self::AUTH_USER_EMAIL);
        $this->loadFixture(TransactionFixture::class, ['sender_id' => $user->getId()], count:5, append: true);
        $lastTransaction = $this->transactionsRepository->findAll()[rand(0,4)];

        $this->client->request('GET', '/api/transactions/' . $lastTransaction->getId());

        $this->assertSame(Response::HTTP_OK, $this->client->getResponse()->getStatusCode());
    }

    public function testGetTransactionDetailsNotFound(): void
    {
        $this->loginUser();

        $this->client->request('GET', '/api/transactions/' . 99999);

        $this->assertSame(Response::HTTP_NOT_FOUND, $this->client->getResponse()->getStatusCode());
    }

    public function testGetTransactionDetailsUnauthorized(): void
    {
        $this->client->request('GET', '/api/transactions/' . 99999);

        $this->assertSame(Response::HTTP_UNAUTHORIZED, $this->client->getResponse()->getStatusCode());
    }

    public function testCreateTransactionSuccessForAdmin(): void
    {
        $this->loginUser();
        $senderInitialState = $this->userRepository->findOneByEmail(self::AUTH_USER_EMAIL);

        $this->loadFixture(UserFixture::class, ['email' => 'receiver@gmail.com'], append: true);
        $receiverInitialState = $this->userRepository->findOneByEmail('receiver@gmail.com');

        $amount = 100.00;
        $this->client->request('POST', '/api/transactions',
            server: ['CONTENT_TYPE' => 'application/json'],
            content: json_encode(['receiverId' => $receiverInitialState->getId(), 'amount' => $amount])
        );

        $sender = $this->userRepository->findOneByEmail(self::AUTH_USER_EMAIL);

        $this->assertSame(Response::HTTP_CREATED, $this->client->getResponse()->getStatusCode());
        $this->assertEquals($senderInitialState->getBalance() - $amount, $sender->getBalance());
        $this->assertEquals($receiverInitialState->getBalance(), User::COST_FOR_REGISTRATION + $amount);
    }

    public function testCreateTransactionValidationFailure(): void
    {
        $this->loginUser();

        $this->client->request('POST', '/api/transactions',
            server: ['CONTENT_TYPE' => 'application/json'],
            content: json_encode(['receiverId' => 999, 'amount' => User::COST_FOR_REGISTRATION + rand(100, 1000)])
        );

        $this->assertSame(Response::HTTP_UNPROCESSABLE_ENTITY, $this->client->getResponse()->getStatusCode());
    }

    public function testCreateTransactionUnauthorized(): void
    {
        $this->client->request('POST', '/api/transactions');

        $this->assertSame(Response::HTTP_UNAUTHORIZED, $this->client->getResponse()->getStatusCode());
    }
}