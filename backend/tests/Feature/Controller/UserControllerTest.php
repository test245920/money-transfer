<?php

namespace App\Tests\Feature\Controller;

use App\DataFixtures\UserFixture;
use App\Entity\User;
use App\Repository\UserRepository;
use App\Tests\Feature\WebTestCase;
use Symfony\Component\HttpFoundation\Response;

class UserControllerTest extends WebTestCase
{
    protected UserRepository $userRepository;
    protected function setUp(): void
    {
        parent::setUp();

        $this->userRepository = $this->container->get(UserRepository::class);
    }

    public function testGetUsersListForAdmin(): void
    {
        $this->loginAdmin();

        $this->loadFixture(UserFixture::class, [
            'roles' => [User::ROLE_USER],
            'banned' => true
        ], count: 9);

        $this->client->request('GET', '/api/users');

        $responseData = json_decode($this->client->getResponse()->getContent(), true);

        $this->assertResponseIsSuccessful();
        $this->assertIsArray($responseData);
        $this->assertArrayHasKey('data', $responseData);
        $this->assertCount(9, $responseData['data']);

        foreach ($responseData['data'] as $userResponse) {
            $this->assertArrayHasKey('id', $userResponse);
            $this->assertArrayHasKey('name', $userResponse);
            $this->assertArrayHasKey('email', $userResponse);
            $this->assertArrayHasKey('balance', $userResponse);
            $this->assertArrayHasKey('roles', $userResponse);
            $this->assertArrayHasKey('banned', $userResponse);
            $this->assertArrayHasKey('createdAt', $userResponse);
            $this->assertArrayHasKey('updatedAt', $userResponse);
        }
    }

    public function testGetUsersListForUser(): void
    {
        $this->loginUser();

        $this->loadFixture(UserFixture::class, [
            'roles' => [User::ROLE_USER],
            'banned' => false
        ], count: 3);

        $mockedUserData = $this->userRepository->getUsers();

        $this->client->request('GET', '/api/users');

        $responseData = json_decode($this->client->getResponse()->getContent(), true);

        $this->assertResponseIsSuccessful();
        $this->assertIsArray($responseData);
        $this->assertArrayHasKey('data', $responseData);
        $this->assertCount(3, $responseData['data']);

        foreach ($responseData['data'] as $index => $userResponse) {
            $this->assertArrayHasKey('id', $userResponse);
            $this->assertArrayHasKey('name', $userResponse);
            $this->assertEquals($mockedUserData[$index]->getId(), $userResponse['id']);
            $this->assertEquals($mockedUserData[$index]->getName(), $userResponse['name']);
            $this->assertEquals($mockedUserData[$index]->getEmail(), $userResponse['email']);
        }
    }

    public function testGetUsersListValidationFailed(): void
    {
        $this->loginUser();

        $this->client->request('GET', '/api/users',['maxBalance' => 'string']);

        $response = $this->client->getResponse();
        $responseData = json_decode($response->getContent(), true);

        $this->assertSame(Response::HTTP_UNPROCESSABLE_ENTITY, $response->getStatusCode());
        $this->assertArrayHasKey('message', $responseData);
        $this->assertArrayHasKey('errors', $responseData);
    }

    public function testGetUsersListUnauthorized(): void
    {
        $this->client->request('GET', '/api/users');

        $this->assertSame(Response::HTTP_UNAUTHORIZED, $this->client->getResponse()->getStatusCode());
    }

    public function testUpdateUserSuccess(): void
    {
        $this->loginAdmin();
        $this->loadFixture(UserFixture::class, ['email' => 'updated@example.com']);

        $user = $this->userRepository->findOneByEmail('updated@example.com');

        $userData = ['name' => 'Updated Name', 'balance' => 526.85];

        $this->client->request('PUT', '/api/users/' . $user->getId(), $userData);

        $response = $this->client->getResponse();
        $responseData = json_decode($response->getContent(), true);

        $this->assertSame(Response::HTTP_OK, $response->getStatusCode());
        $this->assertIsArray($responseData);
        $this->assertArrayHasKey('data', $responseData);
        $this->assertEquals($responseData['data']['name'], $userData['name']);
        $this->assertEquals($responseData['data']['balance'], $userData['balance']);
    }

    public function testUpdateUserValidationFailed(): void
    {
        $this->loginAdmin();
        $this->loadFixture(UserFixture::class, ['email' => 'updated@example.com']);

        $user = $this->userRepository->findOneByEmail('updated@example.com');

        $userData = ['balance' => 'string'];

        $this->client->request('PUT', '/api/users/' . $user->getId(), $userData);

        $response = $this->client->getResponse();
        $responseData = json_decode($response->getContent(), true);

        $this->assertSame(Response::HTTP_UNPROCESSABLE_ENTITY, $response->getStatusCode());
        $this->assertArrayHasKey('message', $responseData);
        $this->assertArrayHasKey('errors', $responseData);
    }

    public function testUpdateUserNotFound(): void
    {
        $this->loginAdmin();

        $this->client->request('PUT', '/api/users/' . 9999);

        $this->assertSame(Response::HTTP_NOT_FOUND, $this->client->getResponse()->getStatusCode());
    }

    public function testUpdateUserUnauthorized(): void
    {
        $this->client->request('PUT', '/api/users/' . 9999);

        $this->assertSame(Response::HTTP_UNAUTHORIZED, $this->client->getResponse()->getStatusCode());
    }

    public function testGetUserDetailsSuccessForAdmin(): void
    {
        $this->loginAdmin();
        $this->loadFixture(UserFixture::class, ['email' => 'test@example.com']);

        $user = $this->userRepository->findOneByEmail('test@example.com');

        $this->client->request('GET', '/api/users/' . $user->getId());

        $response = $this->client->getResponse();

        $this->assertSame(Response::HTTP_OK, $response->getStatusCode());
    }

    public function testGetUserDetailsSuccessForUser(): void
    {
        $this->loginUser();

        $user = $this->userRepository->findOneByEmail(self::AUTH_USER_EMAIL);

        $this->client->request('GET', '/api/users/' . $user->getId());

        $this->assertSame(Response::HTTP_OK, $this->client->getResponse()->getStatusCode());
    }

    public function testGetUserDetailsNotFound(): void
    {
        $this->loginUser();

        $this->client->request('GET', '/api/users/' . 99999);

        $this->assertSame(Response::HTTP_NOT_FOUND, $this->client->getResponse()->getStatusCode());
    }

    public function testGetUserDetailsUnauthorized(): void
    {
        $this->client->request('GET', '/api/users/' . 99999);

        $this->assertSame(Response::HTTP_UNAUTHORIZED, $this->client->getResponse()->getStatusCode());
    }
}
