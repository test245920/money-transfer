<?php

namespace App\Tests\Feature\Controller;

use App\DataFixtures\UserFixture;
use App\Tests\Feature\WebTestCase;
use Symfony\Component\HttpFoundation\Response;

class AuthControllerTest extends WebTestCase
{
    public function testRegisterUser(): void
    {
        $fixture = new UserFixture(['email' => 'test@example.com']);

        $userData = $fixture->make()
            ->only(['name', 'email', 'password', 'password_confirmation'])
            ->toArray();

        $this->client->request('POST', '/api/auth/register', $userData);

        $this->assertEquals(Response::HTTP_CREATED,  $this->client->getResponse()->getStatusCode());

        $responseData = json_decode( $this->client->getResponse()->getContent(), true);

        $this->assertArrayHasKey('data', $responseData);

        $data = $responseData['data'];

        $this->assertArrayHasKey('id', $data);
        $this->assertArrayHasKey('name', $data);
        $this->assertArrayHasKey('email', $data);
        $this->assertArrayHasKey('balance', $data);
        $this->assertArrayHasKey('roles', $data);
        $this->assertArrayHasKey('banned', $data);
        $this->assertArrayHasKey('createdAt', $data);
        $this->assertArrayHasKey('updatedAt', $data);

        $this->assertEquals('test@example.com', $data['email']);
    }

    public function testRegisterUserValidation(): void
    {
        $fixture = new UserFixture(['email' => 'invalid-email']);

        $userData = $fixture->make()
            ->only(['name', 'email', 'password', 'password_confirmation'])
            ->toArray();

        $this->client->request('POST', '/api/auth/register', $userData);

        $this->assertEquals(Response::HTTP_UNPROCESSABLE_ENTITY,  $this->client->getResponse()->getStatusCode());

        $responseData = json_decode( $this->client->getResponse()->getContent(), true);

        $this->assertArrayHasKey('message', $responseData);
        $this->assertArrayHasKey('errors', $responseData);
    }

    public function testGetUser(): void
    {
        $this->loginUser();

        $this->client->request('GET', '/api/auth/user');

        $response = $this->client->getResponse();

        $this->assertEquals(Response::HTTP_OK, $response->getStatusCode());
        $this->assertJson($response->getContent());

        $data = json_decode($response->getContent(), true);

        $this->assertArrayHasKey('data', $data);
        $this->assertArrayHasKey('id', $data['data']);
        $this->assertArrayHasKey('name', $data['data']);
        $this->assertArrayHasKey('email', $data['data']);
    }
}
