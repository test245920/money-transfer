<?php

namespace App\Tests\Feature;

use App\DataFixtures\UserFixture;
use App\Entity\User;
use App\Repository\UserRepository;
use Symfony\Bundle\FrameworkBundle\KernelBrowser;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase as TestCase;
use Symfony\Component\DependencyInjection\ContainerInterface;

class WebTestCase extends TestCase
{
    protected const AUTH_USER_EMAIL = 'authtestuser@gmail.com';

    protected ContainerInterface $container;
    protected KernelBrowser $client;

    protected function setUp(): void
    {
        $this->client = static::createClient();

        $this->container = static::getContainer();
    }

    protected function loadFixture(string $fixtureClassPath, array $data = [], int $count = 1, bool $append = false): void
    {
        $fixtures = [new $fixtureClassPath($data, $count)];

        $fixtureLoader = $this->container->get(FixtureLoader::class);
        $fixtureLoader->loadFixtures($fixtures, $append);
    }

    private function login(string $userRole): void
    {
        $this->loadFixture(UserFixture::class, [
            'email' => self::AUTH_USER_EMAIL,
            'roles' => [$userRole]
        ]);

        $userRepository = $this->container->get(UserRepository::class);
        $testUser = $userRepository->findOneByEmail(self::AUTH_USER_EMAIL);

        $this->client->loginUser($testUser);
    }

    public function loginAdmin(): void
    {
        $this->login(User::ROLE_ADMIN);
    }

    public function loginUser(): void
    {
        $this->login(User::ROLE_USER);
    }
}
