<?php

namespace Symfony\Component\DependencyInjection\Loader\Configurator;

use App\EventListener\CustomValidationExceptionListener;
use App\Tests\Feature\FixtureLoader;

return function(ContainerConfigurator $containerConfigurator) {

    $services = $containerConfigurator->services()
        ->defaults()
        ->autowire()      // Automatically injects dependencies in your services.
        ->autoconfigure(); // Automatically registers your services as commands, event subscribers, etc.

    // makes classes in src/ available to be used as services
    // this creates a service per class whose id is the fully-qualified class name
    $services->load('App\\', '../src/')
        ->exclude('../src/{DependencyInjection,Entity,Kernel.php}');

    $containerConfigurator->services()
        ->set(CustomValidationExceptionListener::class)
        ->tag('kernel.event_listener', ['event' => 'kernel.exception']);

    // order is important in this file because service definitions
    // always *replace* previous ones; add your own service configuration below

    $services->set(FixtureLoader::class)
        ->args(['$entityManager' => service('doctrine.orm.entity_manager')])
        ->public();
};