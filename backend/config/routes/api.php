<?php

use App\Controller\AuthController;
use App\Controller\UserController;
use App\Controller\TransactionController;
use Symfony\Component\Routing\Loader\Configurator\RoutingConfigurator;

return function (RoutingConfigurator $routes) {
    $routes
        ->add('products_index', '/api/products')
        ->controller([AuthController::class, 'index'])
        ->methods(['GET', 'HEAD'])
        ->requirements(['product' => '\d+']);

    $routes
        ->add('api.auth_register', '/api/auth/register')
        ->controller([AuthController::class, 'register'])
        ->methods(['POST']);

    $routes
        ->add('api.auth_user', '/api/auth/user')
        ->controller([AuthController::class, 'user'])
        ->methods(['GET']);

    $routes
        ->add('api.auth_logout', '/api/auth/logout')
        ->controller([AuthController::class, 'logout'])
        ->methods(['DELETE']);



    $routes
        ->add('api.users', '/api/users')
        ->controller([UserController::class, 'index'])
        ->methods(['GET']);
    $routes
        ->add('api.users_update', '/api/users/{user}')
        ->controller([UserController::class, 'update'])
        ->methods(['PUT']);
    $routes
        ->add('api.users_show', '/api/users/{user}')
        ->controller([UserController::class, 'show'])
        ->methods(['GET']);


    $routes
        ->add('api.transactions_list', '/api/transactions')
        ->controller([TransactionController::class, 'index'])
        ->methods(['GET']);
    $routes
        ->add('api.transactions_create', '/api/transactions')
        ->controller([TransactionController::class, 'create'])
        ->methods(['POST']);
    $routes
        ->add('api.transactions', '/api/transactions/{transaction}')
        ->controller([TransactionController::class, 'show'])
        ->methods(['GET']);
};