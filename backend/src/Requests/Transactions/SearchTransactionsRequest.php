<?php

namespace App\Requests\Transactions;

use App\Entity\Transactions;
use App\Requests\FormRequest;
use Symfony\Component\Validator\Constraints as Assert;

class SearchTransactionsRequest extends FormRequest
{
    protected string $entity = Transactions::class;

    protected function constraints(): Assert\Collection
    {
        return new Assert\Collection([
            'senderId' => [
                new Assert\Optional([
                    new Assert\Type(type: 'int'),
                ])
            ],
            'receiverId' => [
                new Assert\Optional([
                    new Assert\Type(type: 'int'),
                ])
            ],
            'minAmount' => [
                new Assert\Optional([
                    new Assert\Type(type: 'numeric'),
                    new Assert\Range(min: 0, max: 99999999.99),
                    new Assert\LessThanOrEqual((float) $this->get('maxAmount'))
                ]),
            ],
            'maxAmount' => [
                new Assert\Optional([
                    new Assert\Type(type: 'numeric'),
                    new Assert\Range(min: 0, max: 99999999.99),
                    new Assert\GreaterThanOrEqual((float) $this->get('minAmount'))
                ]),
            ],
            'createdFrom' => [
                new Assert\Optional([
                    new Assert\Date(),
                    new Assert\LessThanOrEqual((string) $this->get('createdTo'))
                ]),
            ],
            'createdTo' => [
                new Assert\Optional([
                    new Assert\Date(),
                    new Assert\GreaterThanOrEqual((string) $this->get('createdFrom'))
                ]),
            ],
        ]);
    }
}