<?php

namespace App\Requests\Transactions;

use App\Entity\Transactions;
use App\Entity\User;
use App\Requests\FormRequest;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Context\ExecutionContextInterface;

class TransactionCreateRequest extends FormRequest
{
    protected string $entity = Transactions::class;

    protected function constraints(): Assert\Collection
    {
        return new Assert\Collection([
            'senderId' => [
                new Assert\Optional([
                    new Assert\Type(type: 'int'),
                    new Assert\Callback(function ($senderId, ExecutionContextInterface $context) {
                        $senderUser = $this->repository(User::class)->findOneBy(['id' => $senderId]);
                        if (is_null($senderUser) || +$senderUser->getId() === +$this->get('receiverId')) {
                            $context->buildViolation('Invalid sender ID.')
                                ->atPath('sender_id')
                                ->addViolation();
                        }
                    }),
                ]),
            ],
            'receiverId' => [
                new NotBlank(),
                new Assert\Type(type: 'int'),
                new Assert\Callback(function ($receiverId, ExecutionContextInterface $context) {
                    $receiverUser = $this->repository(User::class)->findOneBy(['id' => $receiverId]);
                    if (is_null($receiverUser) || +$receiverUser->getId() === +$this->security->getUser()->getId()) {
                        $context->buildViolation('Invalid receiver ID.')
                            ->atPath('receiverId')
                            ->addViolation();
                    }
                }),
            ],
            'amount' => [
                new NotBlank(),
                new Assert\Type(type: 'numeric'),
                new Assert\Callback(function ($amount, ExecutionContextInterface $context) {
                    $userBalance = $this->security->getUser()->getBalance();
                    if ((float)$amount > (float)$userBalance) {
                        $context->buildViolation('Insufficient balance․')
                            ->atPath('amount')
                            ->addViolation();
                    }
                }),
            ],
        ]);
    }
}