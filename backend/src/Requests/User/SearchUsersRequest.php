<?php

namespace App\Requests\User;

use App\Entity\User;
use App\Requests\FormRequest;
use Symfony\Component\Validator\Constraints as Assert;

class SearchUsersRequest extends FormRequest
{
    protected string $entity = User::class;

    protected function constraints(): Assert\Collection
    {
        return new Assert\Collection([
            'name' => [
                new Assert\Optional([
                    new Assert\Type(type: 'string'),
                    new Assert\Length(min: 3, max: 255)
                ])
            ],
            'email' => [
                new Assert\Optional(new Assert\Email()),
            ],
            'minBalance' => [
                new Assert\Optional([
                    new Assert\Type(type: 'numeric'),
                    new Assert\Range(min: 0, max: 99999999.99),
                    new Assert\LessThanOrEqual((float) $this->get('maxBalance'))
                ]),
            ],
            'maxBalance' => [
                new Assert\Optional([
                    new Assert\Type(type: 'numeric'),
                    new Assert\Range(min: 0, max: 99999999.99),
                    new Assert\GreaterThanOrEqual((float) $this->get('minBalance'))
                ]),
            ],
            'blocked' => [
                new Assert\Optional(new Assert\Choice(choices: ['0','1'])),
            ],
            'fromDate' => [
                new Assert\Optional([
                    new Assert\Date(),
                    new Assert\LessThanOrEqual((string) $this->get('toDate'))
                ]),
            ],
            'toDate' => [
                new Assert\Optional([
                    new Assert\Date(),
                    new Assert\GreaterThanOrEqual((string) $this->get('fromDate'))
                ]),
            ],
        ]);
    }
}
