<?php

namespace App\Requests\User;

use App\Entity\User;
use App\Requests\FormRequest;
use App\Validator\AssertUnique;
use Symfony\Component\Validator\Constraints as Assert;

class UserUpdateRequest extends FormRequest
{
    protected string $entity = User::class;

    protected function constraints(): Assert\Collection
    {
        return new Assert\Collection([
            'name' => [
                new Assert\Optional([
                    new Assert\Type(type: 'string'),
                    new Assert\Length(min: 5, max: 255),
                ])
            ],
            'email' => [
                new Assert\Optional([
                    new Assert\Email(),
                    new AssertUnique(['entity' => $this->entity, 'ignore' => $this->currentRequest->attributes->get('user')]),
                ])
            ],
            'balance' => [
                new Assert\Optional([
                    new Assert\Type(type: 'numeric'),
                    new Assert\Range(min: 0, max: 99999999.99)
                ])
            ],
            'isBanned' => [
                new Assert\Optional([
                    new Assert\Type(type: 'boolean'),
                ])
            ],
        ]);
    }
}