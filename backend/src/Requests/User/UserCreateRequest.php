<?php

namespace App\Requests\User;

use App\Entity\User;
use App\Requests\FormRequest;
use App\Validator\AssertUnique;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\Constraints\NotBlank;

class UserCreateRequest extends FormRequest
{
    protected string $entity = User::class;

    protected function constraints(): Assert\Collection
    {
        return new Assert\Collection([
            'name' => [
                new NotBlank(),
                new Assert\Type(type: 'string'),
                new Assert\Length(min: 3, max: 255),
            ],
            'email' => [
                new NotBlank(),
                new Assert\Email(),
                new AssertUnique(['entity' => $this->entity, 'ignore' => null]),
            ],
            'password' => [
                new NotBlank(),
                new Assert\Length(min: 8),
            ],
            'password_confirmation' => [
                new NotBlank(),
                new Assert\EqualTo(propertyPath: 'password'),
            ],
        ]);
    }
}