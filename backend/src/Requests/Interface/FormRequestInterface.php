<?php

namespace App\Requests\Interface;

use Doctrine\ORM\EntityRepository;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Validator\ConstraintViolationList;

interface FormRequestInterface
{
    public function repository(): EntityRepository;

    public function validate(string $errorMessage = null, int $errorStatus = null): ?JsonResponse;

    public function getErrors(): ConstraintViolationList;
    public function getErrorsMessages(): array;
    public function isApiRequest(): bool;

    public function get(string|array|null $keys = null): array|string|null;
}