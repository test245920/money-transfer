<?php

namespace App\Requests;

use App\Exceptions\CustomValidationException;
use App\Requests\Interface\FormRequestInterface;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\EntityRepository;
use Symfony\Bundle\SecurityBundle\Security;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\Validator\ConstraintViolation;
use Symfony\Component\Validator\ConstraintViolationList;
use Symfony\Component\Validator\Validator\ValidatorInterface;

abstract class FormRequest extends RequestStack implements FormRequestInterface
{
    protected string $entity;
    private ConstraintViolationList $errors;
    private array $errorsWithMessages = [];
    protected Request $currentRequest;

    protected bool $autoValidateRequest = true;

    private static ?array $requestBody = [];

    public function __construct(
        protected ValidatorInterface     $validator,
        protected EntityManagerInterface $entityManager,
        private readonly RequestStack    $requestStack,
        protected Security               $security
    ) {
        $this->currentRequest = $this->requestStack->getCurrentRequest();
        $this->populate();
        $this->validate();
    }

    abstract protected function constraints();

    public function repository(string $entity = null): EntityRepository
    {
        return $this->entityManager->getRepository($entity ?: $this->entity);
    }

    public function validate(string $errorMessage = null, int $errorStatus = null): ?JsonResponse
    {
        $this->errors = $this->validator->validate(self::$requestBody, $this->constraints());

        $count = $this->errors->count();
        if ($count > 0) {
            foreach ($this->errors as $error) {
                if ($property = getPropertyName($error)) {
                    $this->errorsWithMessages[$property][] = $error->getMessage();
                }
            }

            if ($this->isApiRequest() && $this->autoValidateRequest) {
                throw new CustomValidationException($errorMessage, $this->errorsWithMessages, $errorStatus);
            }
        }

        return null;
    }

    public function getErrors(): ConstraintViolationList
    {
        return $this->errors;
    }

    public function getErrorsMessages(): array
    {
        return $this->errorsWithMessages;
    }

    private function populate(): void
    {
        $contentType = $this->currentRequest->headers->get('Content-Type');

        self::$requestBody = ($contentType === 'application/json')
            ? json_decode($this->currentRequest->getContent(), true)
            : array_merge($this->currentRequest->request->all(), $this->currentRequest->query->all());

    }

    public function get(string|array|null $keys = null): array|string|null
    {
        if (is_string($keys) && !empty($keys)) {
            return self::$requestBody[$keys] ?? null;
        }

        $keys = (array) $keys;

        $keys = array_filter($keys);
        if (empty($keys)) {
            return self::$requestBody;
        }

        return array_filter(self::$requestBody, function ($key) use ($keys) {
            return in_array($key, $keys);
        }, ARRAY_FILTER_USE_KEY);
    }

    public function isApiRequest(): bool
    {
        return startsWith($this->currentRequest->getPathInfo(), '/api/');
    }
}

function startsWith(string $haystack, string $needle): bool
{
    return str_starts_with($haystack, $needle);
}

function getPropertyName(ConstraintViolation $constraintViolation): ?string
{
    if (preg_match('/\[(.*?)]/', $constraintViolation->getPropertyPath(), $matches)) {
        return $matches[1];
    }

    return null;
}