<?php

namespace App\Services;

use App\Entity\BaseEntity;
use App\Entity\Transactions;
use App\Exceptions\CustomValidationException;
use App\Repository\TransactionsRepository;
use App\Repository\UserRepository;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use Symfony\Bundle\SecurityBundle\Security;

final class TransactionService
{
    public function __construct(
        private readonly Security $security,
        private readonly UserRepository $userRepository,
        private readonly TransactionsRepository $transactionsRepository,
        private readonly EntityManagerInterface $entityManager
    )
    {}

    /**
     * @param array $payload
     * @return BaseEntity
     */
    public function applyTransaction(array $payload): BaseEntity
    {
        $this->entityManager->beginTransaction();

        try {
            $senderId = $this->security->getUser()->isAdmin()
                ? $payload['senderId']
                : $this->security->getUser()->getId();

            if (empty($senderId)) {
                throw new CustomValidationException(errors: ['senderId' => 'Sender id required for admin']);
            }

            $this->userRepository->decreaseUserBalance($senderId, $payload['amount']);
            $this->userRepository->increaseUserBalance($payload['receiverId'], $payload['amount']);

            $transaction = Transactions::create([
                'senderId' => $senderId,
                'receiverId' => $payload['receiverId'],
                'amount' => $payload['amount']
            ]);

            $this->entityManager->commit();

            return $this->transactionsRepository->save($transaction, true);
        } catch (Exception $exception) {
            $this->entityManager->rollback();

            throw new CustomValidationException(
                message: $exception->getMessage(),
                statusCode: $exception->getCode()
            );
        }
    }
}