<?php

namespace App\Services;

use App\Entity\BaseEntity;
use App\Entity\User;
use App\Repository\UserRepository;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;

final class UserService
{
    public function __construct(
        private readonly UserRepository $userRepository,
        private readonly UserPasswordHasherInterface $userPasswordHasher
    )
    { }

    public function createUser($name, $email, $password): BaseEntity
    {
        $user = User::create([
            'name' => $name,
            'email' => $email,
            'balance' => User::COST_FOR_REGISTRATION,
            'is_banned' => false
        ]);

        $hashPassword = $this->userPasswordHasher->hashPassword($user, $password);
        $user->setPassword($hashPassword);

        return $this->userRepository->save($user, true);
    }
}