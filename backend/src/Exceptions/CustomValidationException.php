<?php

namespace App\Exceptions;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\HttpException;

class CustomValidationException extends HttpException
{
    protected const ERROR_MESSAGE = 'The given data is invalid';

    public function __construct(string $message = null, private array $errors = [], int $statusCode = null)
    {
        parent::__construct(
            $statusCode ?: Response::HTTP_UNPROCESSABLE_ENTITY,
            $message ?: self::ERROR_MESSAGE
        );
    }

    public function getErrors(): array
    {
        return $this->errors;
    }
}