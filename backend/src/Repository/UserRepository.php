<?php

namespace App\Repository;

use App\Entity\User;
use App\Exceptions\CustomValidationException;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;

/**
 * @extends ServiceEntityRepository<User>
 *
 * @method User|null find($id, $lockMode = null, $lockVersion = null)
 * @method User|null findOneBy(array $criteria, array $orderBy = null)
 * @method User[]    findAll()
 * @method User[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class UserRepository extends BaseRepository
{
    protected static string $entityClass = User::class;

    public function getUsers(array $filter = []): array
    {
        $queryBuilder = $this->createQueryBuilder('u');
        $authUser = $this->security->getUser();

        $queryBuilder
            ->where($queryBuilder->expr()->like('u.roles', ':role'))
            ->setParameter('role', '%ROLE_USER%');

        if ($authUser->isUser()) {
            $queryBuilder
                ->andWhere('u.id != :userId')
                ->andWhere('u.is_banned = :blocked')
                ->setParameter('userId', $authUser->getId())
                ->setParameter('blocked', false);
        } else {

            if (isset($filter['blocked'])) {
                $queryBuilder
                    ->andWhere('u.is_banned = :blocked')
                    ->setParameter('blocked', (bool)$filter['blocked']);
            }
        }

        if (isset($filter['minBalance']) && isset($filter['maxBalance'])) {
            $queryBuilder
                ->andWhere('u.balance >= :minBalance')
                ->andWhere('u.balance <= :maxBalance')
                ->setParameter('maxBalance', $filter['maxBalance'])
                ->setParameter('minBalance', $filter['minBalance']);
        }

        if (isset($filter['fromDate']) && isset($filter['toDate'])) {
            $queryBuilder
                ->andWhere('u.created_at >= :fromDate')
                ->andWhere('u.created_at <= :toDate')
                ->setParameter('toDate', $filter['toDate'])
                ->setParameter('fromDate', $filter['fromDate']);
        }

        if (isset($filter['email'])) {
            $queryBuilder
                ->andWhere('u.email LIKE :email')
                ->setParameter('email', '%' . $filter['email'] . '%');
        }

        if (isset($filter['name'])) {
            $queryBuilder
                ->andWhere('u.name LIKE :name')
                ->setParameter('name', '%' . $filter['name'] . '%');
        }

        return $queryBuilder
            ->orderBy('u.id', 'ASC')
            ->getQuery()
            ->getResult();
    }

    public function decreaseUserBalance(int $userId, float $amount): void
    {
        $user = $this->find($userId);

        if (!$user) {
           throw new CustomValidationException(message: "There is no user with specified Id - $userId");
        }

        $user->setBalance($user->getBalance() - $amount);

        $entityManager = $this->getEntityManager();
        $entityManager->persist($user);
        $entityManager->flush();
    }

    public function increaseUserBalance(int $userId, float $amount): void
    {
        $user = $this->find($userId);

        if (!$user) {
            throw new CustomValidationException(message: "There is no user with specified Id - $userId");
        }

        $user->setBalance($user->getBalance() + $amount);

        $entityManager = $this->getEntityManager();
        $entityManager->persist($user);
        $entityManager->flush();
    }

    public function update(User $user, array $data): User
    {
        if (isset($data['name'])) {
            $user->setName($data['name']);
        }

        if (isset($data['email'])) {
            $user->setEmail($data['email']);
        }

        if (isset($data['balance'])) {
            $user->setBalance($data['balance']);
        }

        if (isset($data['isBanned'])) {
            $user->setIsBanned($data['isBanned']);
        }

        $this->save($user, true);

        return $user;
    }

    public function findOneByRole(string $role)
    {
        $queryBuilder = $this->createQueryBuilder('u');

        return $queryBuilder
            ->where($queryBuilder->expr()->like('u.roles', ':role'))
            ->setParameter('role', "%$role%")
            ->orderBy('u.id', 'ASC')
            ->getQuery()
            ->getResult()[0];
    }
}
