<?php

namespace App\Repository;

use App\Entity\BaseEntity;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Bundle\SecurityBundle\Security;

abstract class BaseRepository extends ServiceEntityRepository {

    public function __construct(ManagerRegistry $registry, protected Security $security)
    {
        parent::__construct($registry, static::$entityClass);
    }

    public function save(BaseEntity $entity, bool $flush = false): BaseEntity
    {
        $this->getEntityManager()->persist($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }

        return $entity;
    }

    public function remove(BaseEntity $entity, bool $flush = false): void
    {
        $this->getEntityManager()->remove($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }
}