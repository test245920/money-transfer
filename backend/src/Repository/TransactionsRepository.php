<?php

namespace App\Repository;

use App\Entity\Transactions;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;

/**
 * @extends ServiceEntityRepository<Transactions>
 *
 * @method Transactions|null find($id, $lockMode = null, $lockVersion = null)
 * @method Transactions|null findOneBy(array $criteria, array $orderBy = null)
 * @method Transactions[]    findAll()
 * @method Transactions[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class TransactionsRepository extends BaseRepository
{
    protected static string $entityClass = Transactions::class;

    public function transactions(array $filter = [])
    {
        $query = $this->createQueryBuilder('t');
        $authUser = $this->security->getUser();

        $senderId = !$authUser->isAdmin()
            ? $authUser->getId()
            : ($filter['senderId'] ?? null);

        if ($senderId) {
            $query
                ->andWhere('t.sender_id = :senderId')
                ->setParameter('senderId', $senderId);
        }

        if (isset($filter['receiverId'])) {
            $query
                ->andWhere('t.receiver_id = :receiverId')
                ->setParameter('receiverId', $filter['receiverId']);
        }

        if (isset($filter['minAmount']) && isset($filter['maxAmount'])) {
            $query
                ->andWhere('t.amount >= :minAmount')
                ->andWhere('t.amount <= :maxAmount')
                ->setParameter('maxAmount', $filter['maxAmount'])
                ->setParameter('minAmount', $filter['minAmount']);
        }

        if (isset($filter['createdFrom']) && isset($filter['createdTo'])) {
            $query
                ->andWhere('t.created_at >= :createdFrom')
                ->andWhere('t.created_at <= :createdTo')
                ->setParameter('createdTo', $filter['createdTo'])
                ->setParameter('createdFrom', $filter['createdFrom']);
        }

        return $query
            ->orderBy('t.created_at', 'DESC')
            ->getQuery()
            ->getResult();
    }
}
