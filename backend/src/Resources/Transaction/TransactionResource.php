<?php

namespace App\Resources\Transaction;

use App\Repository\UserRepository;
use App\Resources\EntityResource;
use App\Resources\User\UserResource;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;

class TransactionResource extends EntityResource
{
    private UserResource $userResource;
    private UserRepository $userRepository;

    public function __construct(
        NormalizerInterface $normalizer,
        UserRepository $userRepository,
        UserResource $userResource
    ) {
        parent::__construct($normalizer);

        $this->userRepository = $userRepository;
        $this->userResource = $userResource;
    }

    public function normalizeEntity($entity): array
    {
        return [
            'id' => $entity->getId(),
            'sender' => $this->userResource->item($this->userRepository->find($entity->getSenderId())),
            'receiver' => $this->userResource->item($this->userRepository->find($entity->getReceiverId())),
            'amount' => $entity->getAmount(),
            'createdAt' => $entity->getCreatedAt()
        ];
    }

    protected function additional(): array
    {
        return [
            // Additional data can be added here
        ];
    }
}