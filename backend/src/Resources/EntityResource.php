<?php

namespace App\Resources;

use App\Resources\Interface\EntityResourceInterface;
use ReflectionClass;
use ReflectionProperty;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Serializer\Normalizer\AbstractNormalizer;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;

abstract class EntityResource implements EntityResourceInterface
{
    protected NormalizerInterface $normalizer;

    private bool $isCollection = false;

    public function __construct(NormalizerInterface $normalizer)
    {
        $this->normalizer = $normalizer;
    }

    public function collection(array $entities, int $statusCode = null): JsonResponse
    {
        $this->isCollection = true;

        $data = array_map([$this, 'normalizeEntity'], $entities);

        return $this->successResponse($data, $statusCode);
    }

    public function item($entity, int $statusCode = null): JsonResponse
    {
        $this->isCollection = false;

        $data = $this->normalizeEntity($entity);

        return $this->successResponse($data, $statusCode);
    }

    abstract protected function normalizeEntity($entity): array;

    private function successResponse(array $data, int $statusCode = null): JsonResponse
    {
        if ($this->isCollection) {
            $responseData = ['data' => $data] + $this->additional();
        } else {
            $responseData = ['data' => $data];
        }

        $responseData = recursivelyDecodeJsonResponse($responseData);

        return new JsonResponse($responseData, $statusCode ?: Response::HTTP_OK);
    }

    protected function normalize($entity): array
    {
        $attributes = array_map(function (ReflectionProperty $property) {
            return $property->getName();
        }, (new ReflectionClass($entity))->getProperties());

        return $this->normalizer->normalize($entity, null, [
            AbstractNormalizer::ATTRIBUTES => $attributes,
        ]);
    }

    protected function additional(): array
    {
        return [];
    }
}

function recursivelyDecodeJsonResponse($data) {
    if (is_array($data)) {
        foreach ($data as $key => &$value) {
            if ($value instanceof JsonResponse) {
                $jsonString = $value->getContent();
                $decoded = json_decode($jsonString, true);
                if ($decoded !== null) {
                    if (isset($decoded['data']) && count($decoded) === 1) {
                        $value = recursivelyDecodeJsonResponse($decoded['data']);
                    } else {
                        $value = recursivelyDecodeJsonResponse($decoded);
                    }
                }
            } elseif (is_array($value)) {
                $value = recursivelyDecodeJsonResponse($value);
            }
        }
    }
    return $data;
}
