<?php

namespace App\Resources\Interface;

use Symfony\Component\HttpFoundation\JsonResponse;

interface EntityResourceInterface
{
    public function collection(array $entities, int $statusCode = null): JsonResponse;

    public function item($entity, int $statusCode = null): JsonResponse;
}