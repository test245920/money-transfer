<?php

namespace App\Resources\User;

use App\Resources\EntityResource;

class UserResource extends EntityResource
{
    public function normalizeEntity($entity): array
    {
        return [
            'id' => $entity->getId(),
            'name' => $entity->getName(),
            'email' => $entity->getEmail(),
            'balance' => $entity->getBalance(),
            'roles' => $entity->getRoles(),
            'banned' => $entity->getIsBanned(),
            'createdAt' => $entity->getCreatedAt(),
            'updatedAt' => $entity->getUpdatedAt(),
        ];
    }

    protected function additional(): array
    {
        return [
            // Additional data can be added here
        ];
    }
}