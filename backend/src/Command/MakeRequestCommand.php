<?php

namespace App\Command;

use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

#[AsCommand(
    name: 'make:form-request',
    description: 'Creates a new request class',
)]
class MakeRequestCommand extends Command
{
    protected function configure(): void
    {
        $this
            ->setHelp('This command creates a new request class using the specified name and entity')
            ->addArgument('name', InputArgument::REQUIRED, 'The name of the request class')
            ->addOption('entity', mode:InputArgument::OPTIONAL, description: 'The name of the entity class');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $name = $input->getArgument('name');
        $requestClassName = ucfirst($name);
        $entity = $input->getOption('entity');
        if (!$entity) {
            $output->writeln('The --entity option is required.');
            return Command::FAILURE;
        }
        $entityClassName = ucfirst($entity);
        $requestFileDirectoryPath = __DIR__ . '/../Requests/' . $entityClassName;


        if (!is_dir($requestFileDirectoryPath)) {
            if (!mkdir($requestFileDirectoryPath, 0777, true)) {
                die('Failed to create the directory.');
            }
        }

        $requestFilePath = $requestFileDirectoryPath . '/' . $requestClassName . '.php';

        if (file_exists($requestFilePath)) {
            $output->writeln('The request class already exists.');
            return Command::FAILURE;
        }

        file_put_contents($requestFilePath, requestClassCode($entityClassName, $requestClassName));

        $output->writeln('Request class created successfully.');
        return Command::SUCCESS;
    }
}

function requestClassCode(string $entityClassName, string $requestClassName): string
{
    return <<<PHP
<?php

namespace App\Requests\\$entityClassName;

use App\Entity\\$entityClassName;
use App\Requests\FormRequest;
use Symfony\Component\Validator\Constraints as Assert;

class $requestClassName extends FormRequest
{
    protected string \$entity = $entityClassName::class;

    protected function constraints(): Assert\Collection
    {
        return new Assert\Collection([

        ]);
    }
}
PHP;
}
