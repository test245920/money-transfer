<?php

namespace App\Command;

use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

#[AsCommand(
    name: 'make:json-resource',
    description: 'Creates a new json resource class',
)]
class MakeResourceCommand extends Command
{
    protected function configure(): void
    {
        $this
            ->setHelp('This command creates a new resource class with the specified name')
            ->addArgument('name', InputArgument::REQUIRED, 'The name of the resource class');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $name = $input->getArgument('name');
        $resourceClassName = ucfirst($name);
        $folderName = str_replace(['Resource', 'resource'],'', $resourceClassName);
        $resourceFileDirectoryPath = __DIR__ . '/../Resources/' . $folderName;

        if (!is_dir($resourceFileDirectoryPath)) {
            if (!mkdir($resourceFileDirectoryPath, 0777, true)) {
                die('Failed to create the directory.');
            }
        }

        $resourceFilePath = $resourceFileDirectoryPath . '/' . $resourceClassName . '.php';
        if (file_exists($resourceFilePath)) {
            $output->writeln('The resource class already exists.');
            return Command::FAILURE;
        }

        file_put_contents($resourceFilePath, resourceClassCode($folderName, $resourceClassName));

        $output->writeln('Resource class created successfully.');

        return Command::SUCCESS;
    }
}

function resourceClassCode (string $folderName, string $resourceClassName) {
    return <<<PHP
<?php

namespace App\Resources\\$folderName;

use App\Resources\EntityResource;

class $resourceClassName extends EntityResource
{
    public function normalizeEntity(\$entity): array
    {
        return parent::normalize(\$entity);
    }

    protected function additional(): array
    {
        return [
            // Additional data can be added here
        ];
    }
}
PHP;
}
