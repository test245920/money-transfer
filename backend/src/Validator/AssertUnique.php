<?php

namespace App\Validator;

use Symfony\Component\Validator\Constraint;

/**
 * @Annotation
 *
 * @Target({"PROPERTY", "METHOD", "ANNOTATION"})
 */
#[\Attribute(\Attribute::TARGET_PROPERTY | \Attribute::TARGET_METHOD | \Attribute::IS_REPEATABLE)]
class AssertUnique extends Constraint
{
    public string $message = 'The value "{{ value }}" must be unique.';
    public ?string $entity;
    public mixed $ignore;

    public function __construct($options = null)
    {
        parent::__construct($options);

        if (null === $this->entity) {
            throw new \InvalidArgumentException(sprintf('The "entity" must be set for constraint "%s".', __CLASS__));
        }
    }

    public function getRequiredOptions(): array
    {
        return ['entity'];
    }

    public function getTargets(): array|string
    {
        return self::PROPERTY_CONSTRAINT;
    }
}
