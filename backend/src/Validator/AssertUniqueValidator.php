<?php

namespace App\Validator;

use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;
use Symfony\Component\Validator\Exception\UnexpectedTypeException;
use Symfony\Component\Validator\Exception\UnexpectedValueException;

class AssertUniqueValidator extends ConstraintValidator
{
    public function __construct(protected EntityManagerInterface $entityManager)
    {
    }

    public function validate($value, Constraint $constraint)
    {
        if (!$constraint instanceof AssertUnique) {
            throw new UnexpectedTypeException($constraint, AssertUnique::class);
        }

        if (null === $value || '' === $value) {
            return;
        }

        $repository = $this->entityManager->getRepository($constraint->entity);

        if (null === $repository) {
            throw new UnexpectedValueException(
                sprintf('The repository for "%s" entity does not exist.', $constraint->entity),
                'string'
            );
        }

        $column = null;

        if (preg_match('/\[(.*?)\]/', $this->context->getPropertyPath(), $matches)) {
            $column = $matches[1];
        }

        $existingValue = $repository->findOneBy([$column => $value]);

        if ($existingValue && (!$constraint->ignore || +$existingValue->getId() !== +$constraint->ignore)) {
            $this->context->buildViolation($constraint->message)
                ->setParameter('{{ value }}', $value)
                ->addViolation();
        }
    }
}
