<?php

namespace App\DataFixtures;

use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Faker\Factory;
use Tightenco\Collect\Support\Collection;

class UserFixture extends Fixture
{
    public function __construct(
        private readonly array $data = [],
        private readonly int $count = 1
    ) {
    }

    public function load(ObjectManager $manager): User
    {
        $factory = Factory::create();

        for ($i = 0; $i < $this->count; $i++) {
            $user = new User();
            $user->setName($this->data['name'] ?? $factory->name);
            $user->setEmail($this->data['email'] ?? $factory->email);
            $user->setPassword('$2y$13$lv6LW0J8/qHuFrFougMMCOgq865QRMotWGtWMqlMQhGnjOiMom0MS'); //123456789
            $user->setBalance($this->data['balance'] ?? User::COST_FOR_REGISTRATION);
            $user->setIsBanned($this->data['banned'] ?? $factory->boolean());
            $user->setCustomRoles($this->data['roles'] ?? $factory->randomElements(User::ROLES));

            $manager->persist($user);
            $manager->flush();
        }

        return $user;
    }

    public function make(): Collection
    {
        $factory = Factory::create();

        return new Collection([
            'name' => $this->data['name'] ?? $factory->name,
            'email' => $this->data['email'] ?? $factory->email,
            'password' => 'testpassword',
            'password_confirmation' => 'testpassword',
            'balance' => $this->data['balance'] ?? User::COST_FOR_REGISTRATION,
            'is_banned' => $this->data['banned'] ?? $factory->boolean(),
            'roles' => $this->data['roles'] ?? $factory->randomElements(User::ROLES),
        ]);
    }
}
