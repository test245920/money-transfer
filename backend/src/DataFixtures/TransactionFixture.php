<?php

namespace App\DataFixtures;

use App\Entity\Transactions;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Faker\Factory;

class TransactionFixture extends Fixture
{
    public function __construct(
        private readonly array $data = [],
        private readonly int $count = 1
    ) {
    }

    public function load(ObjectManager $manager): Transactions
    {
        $factory = Factory::create();
        $userFixture = new UserFixture();

        for ($i = 0; $i < $this->count; $i++) {
            $transaction = new Transactions();

            $receiver = $userFixture->load($manager);
            $sender = $userFixture->load($manager);

            $transaction->setReceiverId($this->data['receiver_id'] ?? $receiver->getId());
            $transaction->setSenderId($this->data['sender_id'] ?? $sender->getId());
            $transaction->setAmount($this->data['name'] ?? $factory->randomFloat(2, 10, 1000));

            $manager->persist($transaction);
            $manager->flush();
        }

        return $transaction;
    }
}
