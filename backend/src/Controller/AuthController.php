<?php

namespace App\Controller;

use App\Requests\User\UserCreateRequest;
use App\Resources\User\UserResource;
use App\Services\UserService;
use Lexik\Bundle\JWTAuthenticationBundle\Exception\InvalidTokenException;
use Lexik\Bundle\JWTAuthenticationBundle\Security\User\JWTUser;
use RuntimeException;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Lexik\Bundle\JWTAuthenticationBundle\Services\JWTTokenManagerInterface;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;

class AuthController extends AbstractController
{
    public function __construct(
        protected UserService $userService,
        protected UserResource $userResource,
        protected TokenStorageInterface $tokenStorageInterface,
        protected JWTTokenManagerInterface $jwtManager
    ) {
    }

    public function register(
        UserCreateRequest $userCreateRequest
    ): JsonResponse
    {
        $user = $this->userService->createUser(
            $userCreateRequest->get('name'),
            $userCreateRequest->get('email'),
            $userCreateRequest->get('password')
        );

        return $this->userResource->item($user, Response::HTTP_CREATED);
    }

    public function logout(): JsonResponse
    {
        throw new RuntimeException('Don\'t forget to activate logout in security.yaml');
    }

    public function onLogoutSuccess(Request $request): JsonResponse
    {
        $jwtUser = $this->tokenStorageInterface->getToken()?->getUser();
        if (!$jwtUser instanceof JWTUser) {
            throw new InvalidTokenException('Token not found');
        }

        $this->useCase->process($jwtUser);

        return new JsonResponse('Logged out successfully');
    }

    public function user(): JsonResponse
    {
        $this->denyAccessUnlessGranted('ROLE_USER');

        return $this->userResource->item($this->getUser());
    }
}