<?php

namespace App\Controller;

use App\Entity\Transactions;
use App\Requests\Transactions\SearchTransactionsRequest;
use App\Services\TransactionService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use App\Requests\Transactions\TransactionCreateRequest;
use App\Repository\TransactionsRepository;
use App\Resources\Transaction\TransactionResource;

class TransactionController extends AbstractController
{
    public function __construct(
        protected TransactionsRepository $transactionsRepository,
        protected TransactionResource $transactionResource
    ){}

    public function index(SearchTransactionsRequest $searchTransactionsRequest): JsonResponse
    {
        $transactions = $this->transactionsRepository->transactions($searchTransactionsRequest->get());

        return $this->transactionResource->collection($transactions);

    }

    public function create(TransactionCreateRequest $transactionCreateRequest, TransactionService $transactionService):  JsonResponse
    {
        $this->denyAccessUnlessGranted('create', new Transactions(), 'Unauthorized access!');

        $transaction = $transactionService->applyTransaction($transactionCreateRequest->get());

        return $this->transactionResource->item($transaction, Response::HTTP_CREATED);
    }

    public function show(Transactions $transaction): JsonResponse
    {
        $this->denyAccessUnlessGranted('view', $transaction, 'Unauthorized access!');

        return $this->transactionResource->item($transaction);
    }
}
