<?php

namespace App\Controller;

use App\Entity\User;
use App\Requests\User\SearchUsersRequest;
use App\Requests\User\UserUpdateRequest;
use App\Resources\User\UserResource;
use App\Repository\UserRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;

class UserController extends AbstractController
{
    public function __construct(
        protected UserResource $userResource,
        protected UserRepository $userRepository,
    ){}

    public function index(SearchUsersRequest $searchUsersRequest): JsonResponse
    {
        $users = $this->userRepository->getUsers($searchUsersRequest->get());

        return $this->userResource->collection($users);
    }

    public function update(User $user, UserUpdateRequest $userUpdateRequest): JsonResponse
    {
        $this->denyAccessUnlessGranted('edit', $user, 'Unauthorized access!');

        $user = $this->userRepository->update($user, $userUpdateRequest->get());

        return $this->userResource->item($user);
    }

    public function show(User $user): JsonResponse
    {
        $this->denyAccessUnlessGranted('view', $user, 'Unauthorized access!');

        return $this->userResource->item($user);
    }
}
