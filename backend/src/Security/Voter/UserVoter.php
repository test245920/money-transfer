<?php
namespace App\Security\Voter;

use App\Entity\User;
use LogicException;
use Symfony\Bundle\SecurityBundle\Security;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;
use Symfony\Component\Security\Core\User\UserInterface;

class UserVoter extends Voter
{
    const VIEW = 'view';
    const EDIT = 'edit';

    public function __construct(
        private readonly Security $security
    ) {
    }
    protected function getSupportedAttributes(): array
    {
        return [self::VIEW, self::EDIT];
    }

    protected function getSupportedClasses(): array
    {
        return [User::class];
    }


    protected function supports($attribute, $subject): bool
    {
        return $subject instanceof UserInterface
            && in_array($attribute, $this->getSupportedAttributes());
    }

    protected function voteOnAttribute(
        $attribute,
        $subject,
        TokenInterface $token
    ): bool
    {
        switch ($attribute) {
            case self::EDIT:
            case self::VIEW:
                return  $token->getUser()->getId() === $subject->getId()
                    || $this->security->isGranted('ROLE_ADMIN', $token->getUser());
        }

        throw new LogicException('Invalid attribute: '.$attribute);
    }

}