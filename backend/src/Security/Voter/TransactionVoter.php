<?php
namespace App\Security\Voter;

use App\Entity\Transactions;
use Symfony\Bundle\SecurityBundle\Security;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;

class TransactionVoter extends Voter
{
    const VIEW = 'view';
    const EDIT = 'edit';
    const CREATE = 'create';

    public function __construct(
        private Security $security
    ) {
    }
    protected function getSupportedAttributes()
    {
        return array(
            self::VIEW,
            self::EDIT,
            self::CREATE
        );
    }

    protected function getSupportedClasses()
    {
        return array(
            Transactions::class
        );
    }


    protected function supports($attribute, $subject): bool
    {
        return $subject instanceof Transactions
            && in_array($attribute, $this->getSupportedAttributes());
    }

    protected function voteOnAttribute(
        $attribute,
        $transaction,
        TokenInterface $token
    ): bool
    {
        switch ($attribute) {
            case self::CREATE:
                return $this->security->isGranted('ROLE_USER', $token->getUser());
            case self::EDIT:
            case self::VIEW:
                return  $token->getUser()->getId() === $transaction->getSenderId()
                    || $this->security->isGranted('ROLE_ADMIN', $token->getUser());
        }

        throw new \LogicException('Invalid attribute: '.$attribute);
    }

}